use std::io;

fn main() {
    loop {
        print!("rh > ");
        let mut reader = io::stdin();

        let input = reader.read_line().ok().expect("Failed to read line");

        if input.trim() == "exit" {
            break;
        } else {
            println!("processing {}", input);
        }
    }
}
